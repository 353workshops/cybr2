package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	data := []byte(`
	{
		"user": "saitama",
		"action": "punch"
	}
	`)

	/*
		// var j Job
		var m map[string]any
		if err := json.Unmarshal(data, &m); err != nil {
			fmt.Println("ERROR:", err)
			return
		}
		fmt.Printf("got: %#v\n", m)

		if _, ok := m["count"]; ok {
			fmt.Println("got count")
		} else {
			fmt.Println("missing count")
		}

		var j Job
		// mapstructure: map[string]any -> struct
		if err := mapstructure.Decode(m, &j); err != nil {
			fmt.Println("ERROR:", err)
			return
		}
	*/

	j := Job{Count: 1}
	if err := json.Unmarshal(data, &j); err != nil {
		fmt.Println("ERROR:", err)
		return
	}
	fmt.Printf("got: %#v\n", j)
}

/* Zero vs Missing

Question: Did someone send me a 0 or did they didn't send "count"?
Solutions:
- Don't care (design, easiest)
	- Start with defaults
- Use pointers (handle with care, try to avoid, panics ...)
- Use map[string]any (uncomfortable, type assertions ...)
	- mapstructure, you'll decode twice
*/

type Job struct {
	User   string
	Action string
	Count  int
	// Count  *int
}
