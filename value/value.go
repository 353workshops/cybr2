package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"time"
)

func main() {
	n := 42
	data, err := json.Marshal(n)
	if err != nil {
		fmt.Println("ERROR:", err)
		return
	}

	var i any
	if err := json.Unmarshal(data, &i); err != nil {
		fmt.Println("ERROR:", err)
		return
	}
	fmt.Printf("i: %v (%T)\n", i, i)

	enc := json.NewEncoder(os.Stdout)
	enc.Encode(time.Now())
	enc.Encode([]byte("Hello"))

	v := Value{
		Amount: 12.3,
		Unit:   "centimeter",
	}
	enc.Encode(v) // "12.3 centimeter"

	data, err = json.Marshal(v)
	if err != nil {
		fmt.Println("ERROR:", err)
		return
	}

	var v2 Value
	if err := json.Unmarshal(data, &v2); err != nil {
		fmt.Println("ERROR:", err)
		return
	}
	fmt.Println("got:", v2)
}

// Exercise: Implement json.Unmarshaler
// Hint: fmt.Sscanf

func (v *Value) UnmarshalJSON(data []byte) error {
	// s := string(data[1 : len(data)-1]) // remove ""
	r := bytes.NewReader(data[1 : len(data)-1])
	// strings.NewWriter, strings.NewWriter
	// bytes.Buffer

	// Tip: First parse, then assign (read/modify/write)
	var a float64
	var u string
	// if _, err := fmt.Sscanf(s, "%f %s", &a, &u); err != nil {
	if _, err := fmt.Fscanf(r, "%f %s", &a, &u); err != nil {
		return err
	}

	v.Amount = a
	v.Unit = u
	return nil
}

// MarshalJSON implements json.Marshaler.
func (v Value) MarshalJSON() ([]byte, error) {
	// Step 1: Convert to a type JSON known
	s := fmt.Sprintf("%f %s", v.Amount, v.Unit)

	// Step 2: Use json.Marshal
	return json.Marshal(s)

	// Step 3: There is no step 3
}

type Value struct {
	Amount float64 `json:"amount,omitempty"`
	Unit   string  `json:"unit,omitempty"`
}

/* Design Tip
Have a type for each layer of your code (API, Business, Storage)

API: User (very low velocity)     |
Business: User (high velocity)    |
Storage: User (low velocity)      v
*/

/* encoding/json API

- Fields in JSON that are not found in the struct are ignored
- Fields in the struct that are not found in JSON are ignored (zero value)
- Use field tags to modify naming and other features

Problem: Zero vs Missing value

[]byte
	Go -> JSON: Marshal
	JSON -> Go: Unmarshal

io.Writer/io.Reader
	Go -> JSON: NewEncoder
	JSON -> Go: NewDecoder
*/

/* JSON <-> Go Types

bool <-> bool
string <-> string (not nil)
null <-> nil
number <-> float64, float32, int, int8 ... int64, uint ... uint64, complex64, complex128, math/big
array <-> []T, []any
object <-> struct, map[string]any

Not in JSON
time.Time -> string in RFC3339 format
[]byte -> base64 encoded string
*/
