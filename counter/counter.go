package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	//var mu sync.Mutex
	counter := int64(0)

	const size = 10
	var wg sync.WaitGroup
	wg.Add(size)
	for i := 0; i < size; i++ {
		//wg.Add(1)
		go func() {
			defer wg.Done()

			for j := 0; j < 1000; j++ {
				time.Sleep(time.Microsecond)
				/*
					mu.Lock()
					counter++
					mu.Unlock()
				*/
				atomic.AddInt64(&counter, 1)
			}
		}()
	}

	wg.Wait()
	fmt.Println(counter)
}
