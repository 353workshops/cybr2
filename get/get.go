package main

import (
	"context"
	"fmt"
	"net/http"
	"time"
)

func main() {
	// resp, err := http.Get("https://cyberark.com") // without context
	ctx, cancel := context.WithTimeout(context.Background(), time.Second) // time.Millisecond)
	defer cancel()

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "https://cyberark.com", nil)
	if err != nil {
		fmt.Println("ERROR:", err)
		return
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println("ERROR:", err)
		return
	}
	if resp.StatusCode != http.StatusOK {
		fmt.Println("ERROR:", resp.Status)
		return
	}
	defer resp.Body.Close()

	fmt.Println("OK")
}
