package main

import "testing"

var validKeys = map[Key]bool{
	Copper:  true,
	Crystal: true,
	Jade:    true,
}

func isValidKeyMap(key Key) bool {
	return validKeys[key]
}

func Benchmark_isVaildKeySwitch(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ok := isValidKey(Key(17))
		if ok {
			b.Fatal(Key(17))
		}
	}
}

func Benchmark_isVaildKeyMap(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ok := isValidKeyMap(Key(17))
		if ok {
			b.Fatal(Key(17))
		}
	}
}
