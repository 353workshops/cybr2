package main

import (
	"fmt"
)

func main() {
	fmt.Println(NewItem(10, 20))
	fmt.Println(NewItem(10, 2000))

	// i := Item{1000, 2000}
	i, err := NewItem(10, 20)
	if err != nil {
		fmt.Println("ERROR:", err)
		return
	}

	// Aside: Printing
	fmt.Println("i:", i)
	fmt.Printf("i  v: %v\n", i)
	fmt.Printf("i +v: %+v\n", i)
	fmt.Printf("i #v: %#v\n", i)

	a, b := 1, "1"
	// Tip: Use %#v in debug/log prints
	fmt.Printf("a=%#v, b=%#v\n", a, b)

	i.Move(200, 300)
	fmt.Println("i (move):", i)

	p1 := Player{
		Name: "Parzival",
		Item: Item{10, 20},
	}
	fmt.Printf("p1: %#v\n", p1)
	fmt.Println("p1.Item.X:", p1.Item.X)
	fmt.Println("p1.X:", p1.X)
	p1.Move(345, 391)
	fmt.Printf("p1 (move): %#v\n", p1)

	ms := []Mover{
		&p1,
		&Car{Plate: "1783A"},
		&i,
	}

	fmt.Println()
	moveAll(ms, 7, 9)
	for _, m := range ms {
		fmt.Println("m:", m)
	}

	fmt.Println(p1.Found(Key(17))) // error ...
	p1.Found(Copper)
	p1.Found(Copper)
	fmt.Println("keys:", p1.Keys) // [copper]

	/*
		a := 'a'
		fmt.Println(Copper + a)
	*/
}

/* What fmt.Print* does
func print(val any) {
	if s, ok := val.(fmt.Stringer); ok { // type assertion
		_print(s.String())
		return
	}
	// do usual printing
}
*/

// String implements fmt.Stringer
func (k Key) String() string {
	switch k {
	case Copper:
		return "copper"
	case Jade:
		return "jade"
	case Crystal:
		return "crystal"
	}

	return fmt.Sprintf("<Key %d>", k)
}

type Key byte

const (
	Copper Key = iota + 1
	Jade
	Crystal
)

// Exercise:
// Add a field "Keys" to Player which is a slice of strings
// Add a method "Found(key string) error" to Player that will add key to Keys
// - If key is not one of "jade", "copper", "crystal" return error
// - Add a key only once

/*
1. Write down the problem.
2. Think very hard.
3. Write down the solution.
- Feynman's Algorithm

Make it work, make it right, make it fast.
	- Kent Beck
*/

// Interface say what we need, not what we provide
// Interface are small (stdlib avg < 2)
// Accept interfaces, return types

/*
Interface use cases:
- Polymorphism (I want to read, don't who implements Read)
- Change how stdlib treats your types
- Mocking
*/
type Mover interface {
	Move(int, int)
}

func moveAll(ms []Mover, x, y int) {
	for _, m := range ms {
		m.Move(x, y)
	}
}

type Car struct {
	Plate string
	Item
}

func (p *Player) Found(key Key) error {
	if !isValidKey(key) {
		return fmt.Errorf("unknown key: %q", key)
	}

	if !p.hasKey(key) {
		p.Keys = append(p.Keys, key)
	}
	return nil
}

func (p *Player) hasKey(key Key) bool {
	// go 1.21:
	// return slices.Contains(p.Keys, key)
	for _, k := range p.Keys {
		if k == key {
			return true
		}
	}

	return false
}

func isValidKey(key Key) bool {
	// return validKeys[key]
	switch key {
	// case "copper", "crystal", "jade":
	case Copper, Crystal, Jade:
		return true
	}

	return false
}

type Player struct {
	Name string
	Item // Player embeds Item
	Keys []Key
}

// Pointer vs value receiver
// - Use value semantics as much as you can
// - Use value semantics for built in types (e.g. type Flag = int)
// - Use pointer receiver when mutating
// - Do not mix pointer & value semantics in the same type

// "i" is called "the receiver"
func (i *Item) Move(x, y int) {
	i.X = x
	i.Y = y
}

func NewItem(x, y int) (Item, error) {
	if x < 0 || x > maxX || y < 0 || y > maxY {
		return Item{}, fmt.Errorf("%d/%d out of bound %d/%d", x, y, maxX, maxY)
	}

	//i := Item{x, y}
	/*
		var i Item
		i.X, i.Y = x, y
	*/
	i := Item{
		X: x,
		Y: y,
	}
	return i, nil
}

const (
	maxX = 600
	maxY = 400
)

// Item is an item in the game
type Item struct {
	X int
	Y int
}

// go install golang.org/x/tools/cmd/stringer@latest
// export PATH="$(go env GOPATH)/bin:${PATH}"
// stringer -type Key
