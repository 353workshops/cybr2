package tokenizer

import (
	"strings"
)

var (
	//	wordRe   = regexp.MustCompile(`[a-zA-Z]+`)
	suffixes = []string{"ed", "ing", "s"}
)

func init() {
	// runs before main (every time this package is imported)
}

// works, working, worked -> work
func Stem(word string) string {
	for _, s := range suffixes {
		if strings.HasSuffix(word, s) {
			return word[:len(word)-len(s)]
		}
	}

	return word
}

func initialSplit(text string) []string {
	//	return wordRe.FindAllString(text, -1)
	// var fs []string
	// research: number of tokens is about 40% from length of text
	fs := make([]string, 0, len(text)/3)
	i := 0
	for i < len(text) {
		// eat start
		for i < len(text) && !isLetter(text[i]) {
			i++
		}
		if i == len(text) {
			break
		}

		j := i + 1
		for j < len(text) && isLetter(text[j]) {
			j++
		}
		fs = append(fs, text[i:j])

		i = j
	}
	return fs
}

func isLetter(b byte) bool {
	return (b >= 'a' && b <= 'z') || (b >= 'A' && b <= 'Z')
}

// "Who's on first?" -> [who on first]
func Tokenize(text string) []string {
	var tokens []string
	for _, tok := range initialSplit(text) {
		tok = strings.ToLower(tok)
		tok = Stem(tok)
		if tok != "" {
			tokens = append(tokens, tok)
		}
	}
	return tokens
}

/*
Go
type Reader interface {
	Read(d []byte) ( int, error)
}

Python
type Reader interface {
	Read(n int) ([]byte, error)
}
*/
