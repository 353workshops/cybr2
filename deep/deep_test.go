package deep

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestMax(t *testing.T) {
	i, err := Max([]int{3, 1, 2})
	require.NoError(t, err)
	require.Equal(t, 3, i)

	f, err := Max([]float64{3, 1, 2})
	require.NoError(t, err)
	require.Equal(t, 3.0, f)

	_, err = Max[int](nil)
	require.Error(t, err)

	s, err := Max([]string{"B", "C", "A"})
	require.NoError(t, err)
	require.Equal(t, "C", s)

}

func TestReluInt(t *testing.T) {
	iout := Relu(7)
	require.Equal(t, 7, iout)
	fout := Relu(7.0)
	require.Equal(t, 7.0, fout)
	iout = Relu(-2)
	require.Equal(t, 0, iout)
	mout := Relu(time.November)
	require.Equal(t, time.November, mout)
}

/*
func TestReluInt(t *testing.T) {
	out := ReluInt(7)
	require.Equal(t, 7, out)
	out = ReluInt(-2)
	require.Equal(t, 0, out)
}

func TestReluFloat64(t *testing.T) {
	out := ReluInt(7)
	require.Equal(t, 7, out)
	out = ReluInt(-2)
	require.Equal(t, 0, out)
}

*/
