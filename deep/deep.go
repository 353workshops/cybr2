package deep

import "fmt"

// type ID string

type Ordered interface {
	OrderedNum | ~string
}

// Exercise: Write a generic Max function
// It should err if values is empty
func Max[T Ordered](values []T) (T, error) {
	if len(values) == 0 {
		var zero T
		return zero, fmt.Errorf("Max of empty slice")
	}

	/* Don't do this
	switch any(values[0]).(type) {
	case string:
		// ...
	}
	*/

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}

	return m, nil
}

type OrderedNum interface {
	~int | ~float64
}

func Relu[T OrderedNum](n T) T {
	if n < 0 {
		return 0
	}

	return n
}

/*
func ReluInt(n int) int {
	if n < 0 {
		return 0
	}

	return n
}

func ReluFloat64(n float64) float64 {
	if n < 0 {
		return 0
	}

	return n
}

*/
