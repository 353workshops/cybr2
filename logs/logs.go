package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"os"
	"time"
)

func main() {
	// one()
	many()
}

func one() error {
	lr := LogRecord{
		Time:    time.Now().UTC(),
		Level:   "INFO",
		Message: "Server starting",
		Attrs: map[string]string{
			"host": "qa-9",
			"env":  "qa",
		},
	}

	var buf bytes.Buffer
	enc := json.NewEncoder(&buf)
	if err := enc.Encode(lr); err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
	defer cancel()

	r, err := http.NewRequestWithContext(ctx, http.MethodPost, "http://localhost:8080/logs", &buf)
	if err != nil {
		return err
	}

	resp, err := http.DefaultClient.Do(r)
	if err != nil {
		return err
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status - %s", resp.Status)
	}

	io.Copy(os.Stdout, resp.Body)
	return nil
}

func many() error {
	// see also net.Pipe
	r, w, err := os.Pipe()
	if err != nil {
		return err
	}

	go func() {
		defer w.Close()
		enc := json.NewEncoder(w)
		for i := 0; i < 5; i++ {
			lr := LogRecord{
				Time:    time.Now(),
				Level:   "INFO",
				Message: "Server alive",
				Attrs: map[string]string{
					"host": "qa-9",
					"env":  "qa",
				},
			}
			if err := enc.Encode(lr); err != nil {
				slog.Error("can't write JSON", "error", err)
				return
			}
			time.Sleep(100 * time.Millisecond)
		}
	}()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, "http://localhost:8080/logs", r)
	if err != nil {
		return err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status - %s", resp.Status)
	}

	io.Copy(os.Stdout, resp.Body)
	return nil
}

type LogRecord struct {
	Time    time.Time
	Level   string
	Message string
	Attrs   map[string]string
}
