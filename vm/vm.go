package main

import (
	"os"
)

func main() {
	vm := NewVM("3fa999a")
	//fmt.Println(vm)
	display(vm)
}

// go build -gcflags=-m

func display(vm VM) {
	os.Stdout.Write([]byte(vm.ID))
}

// Mechanical Sympathy
// Go: Pass by value as much as you can

// New/Constructor functions
// func NewVM(id string) VM
// func NewVM(id string) *VM
// func NewVM(id string) (VM, error)
// func NewVM(id string) (*VM, error)
func NewVM(id string) VM {
	vm := VM{
		ID:     id,
		Status: Starting,
	}

	return vm
}

type VM struct {
	ID     string
	Status Status
}

const (
	Starting Status = "starting"
	Running  Status = "running"
	Stopped  Status = "stopped"
)

type Status string
