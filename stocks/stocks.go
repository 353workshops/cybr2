package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"time"
)

// modeling a large JSON document
// curl https://api.stocktwits.com/api/2/streams/symbol/CYBR.json

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	counts, err := RelatedStocks(ctx, "CYBR")
	if err != nil {
		/*
			fmt.Println("ERROR:", err)
			return
		*/
		// IRL
		fmt.Fprintf(os.Stderr, "error: %s\n", err)
		os.Exit(1)
		// Rule: Only main can print  and exit the program, all others should log and return errors
	}

	for sym, count := range counts {
		fmt.Printf("%s: %d\n", sym, count)
	}
}

// RelatedStocks returns a map of symbol -> count
func RelatedStocks(ctx context.Context, symbol string) (map[string]int, error) {
	// TODO: HTTP request
	// GoLand: stocks/CYBR.json
	/* DEBUG: Work with file to avoid rate limiting
	file, err := os.Open("CYBR.json")
	if err != nil {
		return nil, err
	}
	defer file.Close()
	r := file
	*/

	r, err := getQuotes(ctx, symbol)
	if err != nil {
		return nil, err
	}
	defer r.Close()

	var reply struct {
		Messages []struct {
			Symbols []struct {
				Name string `json:"symbol"`
			}
		} `json:"messages"`
	}
	if err := json.NewDecoder(r).Decode(&reply); err != nil {
		return nil, err
	}

	counts := make(map[string]int)
	for _, msg := range reply.Messages {
		for _, sym := range msg.Symbols {
			if sym.Name == symbol {
				continue
			}
			counts[sym.Name]++
		}
	}

	return counts, nil
}

func getQuotes(ctx context.Context, symbol string) (io.ReadCloser, error) {
	url, err := url.JoinPath("https://api.stocktwits.com/api/2/streams/symbol", symbol+".json")
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("%q: bad status - %s", url, resp.Status)
	}

	// resp.Header.Get("Content-Type") -> string
	// resp.Header.Values("Content-Type") -> []string

	return resp.Body, nil
}

/*
==== REQUEST ===
->
	GET / HTTP/1.1
	Host: robpike.io

<-
	HTTP/1.1 200 OK
	...
	Transfer-Encoding: chunked
	...
	4
	💩
	4
	💩
	...
	0
==== REQUEST ===

*/
