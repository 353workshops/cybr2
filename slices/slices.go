package main

import (
	"fmt"
	"sort"
)

func main() {
	cart := []string{"apple", "orange", "lemon"}
	fmt.Printf("len: %d, cap: %d\n", len(cart), cap(cart))
	fmt.Println("cart:", cart)
	cart = append(cart, "bread")
	fmt.Printf("len: %d, cap: %d\n", len(cart), cap(cart))
	fmt.Println("cart (append):", cart)
	fruit := cart[:3] // slicing (half open range)
	fmt.Println("fruit:", fruit)
	fruit[0] = "pear"
	fmt.Println("fruit (change):", fruit)
	fmt.Println("cart (change):", cart)

	var values []int
	//	const size = 10_000
	//	values := make([]int, 0, size)

	for i := 0; i < 10_000; i++ {
		values = appendInt(values, i)
	}
	fmt.Println("values (append):", values[:10])
	fmt.Println()

	nums := []float64{3, 1, 2}
	fmt.Println(median(nums)) // 2
	nums = []float64{3, 1, 2, 4}
	fmt.Println(median(nums)) // 2.5
	fmt.Println("nums:", nums)

	fmt.Println()
	s := make([]string, 100)
	//s1 := s[:5]
	s1 := s[:5:5]
	s1[0] = "hi"
	s1 = append(s1, "there")
	fmt.Println(s[:10])
	fmt.Printf("s1: len: %d, cap: %d\n", len(s1), cap(s1))

	var ip [4]byte // ip is an array
	fmt.Printf("%v of %T\n", ip, ip)
}

func median(values []float64) float64 {
	vs := make([]float64, len(values))
	copy(vs, values)
	sort.Float64s(vs)
	i := len(vs) / 2
	if len(vs)%2 == 1 {
		return vs[i]
	}

	m := (vs[i-1] + vs[i]) / 2
	return m
}

func appendInt(values []int, v int) []int {
	i := len(values)
	if len(values) < cap(values) { // there's enough space in underlying array
		values = values[:len(values)+1]
	} else { // need to allocate + copy
		size := 2 * (1 + len(values)) // +1 for nil/empty slice
		fmt.Println(cap(values), "->", size)
		vs := make([]int, size)
		copy(vs, values)
		values = vs[:len(values)+1]
	}

	values[i] = v
	return values
}

// aside: assignment
