package main

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"expvar"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"net/http/pprof"
	"os"
	"strings"
	"time"

	"github.com/google/uuid"
)

func main() {
	// TODO: Validate configuration

	// Build server (dependency injection)
	log := slog.New(slog.NewTextHandler(os.Stdout, nil))
	api := API{
		log: log,
		// TODO: db connection, ...
	}

	// After reading configuration and connecting to database ...
	if err := api.healthCheck(context.Background()); err != nil {
		fmt.Fprintf(os.Stderr, "error: health failed - %s\n", err)
		os.Exit(1)
	}

	// Routing path -> handler
	// 1.22 +: Methods & path parts
	// 1.21 -: /name - exact math, /name/ - prefix math
	// http.HandleFunc("GET /health", healthHandler)
	// http.HandleFunc("GET /record/{id}", healthHandler)
	// Security: Don't use the default server mux, anyone can stick handler on it
	// TODO: Move to buildRoutes function
	mux := http.NewServeMux()
	mux.HandleFunc("GET /health", api.healthHandler)
	mux.HandleFunc("POST /log", api.logHandler)
	mux.Handle("GET /debug/vars", expvar.Handler())
	mux.HandleFunc("GET /debug/pprof/profile", pprof.Profile)

	addr := ":8080"
	// Config: Run your own server with timeouts
	srv := http.Server{
		Addr:    addr,
		Handler: topMiddleware(mux),

		ReadHeaderTimeout: 500 * time.Millisecond,
		ReadTimeout:       10 * time.Second,
		WriteTimeout:      10 * time.Second,
		IdleTimeout:       time.Second,
	}

	// slog 1.22+
	slog.Info("server starting", "address", addr, "pid", os.Getpid())
	if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		fmt.Fprintf(os.Stderr, "error: %s\n", err)
		os.Exit(1)
	}
}

func (a *API) logHandler(w http.ResponseWriter, r *http.Request) {
	vals := a.getCtxValues(r.Context())
	log := a.log.With("id", vals.ID, "path", r.URL.Path)

	// Step 1: Parse & validate info
	var rec Record
	dec := json.NewDecoder(http.MaxBytesReader(w, r.Body, 1_000_000))
	if err := dec.Decode(&rec); err != nil {
		//log.Error("bad record", "error", err)
		log.LogAttrs(
			r.Context(),
			slog.LevelInfo,
			"bad record",
			slog.String("error", err.Error()),
		)

		http.Error(w, "bad record", http.StatusBadRequest)
		return
	}

	if err := rec.Validate(); err != nil {
		log.Error("bad record", "error", err)
		http.Error(w, "bad record", http.StatusBadRequest)
		return
	}

	// Step 2: work
	// TODO: Save to database
	log.Info("new record", "record", rec)
	rid := uuid.NewString()

	// Step 3: Return result
	reply := map[string]any{
		"id": rid,
	}

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusCreated)
	/* 2 options:
	- NewEncoder(w)
		- Not everything in memory
		- Can stream
	- Marshal + w.Write
		- Can check for error before sending
	*/

	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(reply); err != nil {
		log.Error("send JSON", "error", err)
		http.Error(w, "can't write JSON", http.StatusInternalServerError)
		return
	}

	if _, err := io.Copy(w, &buf); err != nil {
		log.Error("write JSON", "error", err)
	}
}

/* Pointer vs Value receiver
Value: Everyone has their own copy of the data, changes are in isolation
Pointer: Everyone has the same data, need to synchronize

- By default use value receiver
- Use pointer receiver if mutating
- Keep same semantics (value/pointer) throughout. Exceptions:
	- Unmarshaling
	- If you have locks (e.g. sync.Mutex)
*/

type API struct {
	log *slog.Logger
}

func needAuth(path string) bool {
	switch {
	case path == "/health":
		return false
	case strings.HasPrefix(path, "/debug/"):
		return false
	}
	return true
}

func topMiddleware(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		// before handlers
		start := time.Now()

		user, passwd, ok := r.BasicAuth() // JWT ...
		// if needAuth(r.URL.Path) {
		if false { // FIXME
			if !ok || !(user == "joe" && passwd == "baz00ka") {
				if !ok {
					// TODO: Pass logger to topMiddleware
					slog.Error("missing auth")
				} else {
					slog.Error("bad auth", "user", user)
				}
				http.Error(w, "bad auth", http.StatusUnauthorized)
				return
			}
		} else {
			user = "<unknown>"
		}

		vals := ctxValues{
			ID:    uuid.NewString(),
			Login: user,
		}
		ctx := context.WithValue(r.Context(), ctxKey, &vals)

		r = r.WithContext(ctx)
		h.ServeHTTP(w, r)

		// after handlers
		duration := time.Since(start)
		slog.Info("timing",
			"path", r.URL.Path,
			"method", r.Method,
			"duration", duration,
			"peer", r.RemoteAddr,
		)
	}

	return http.HandlerFunc(fn)
}

func (a *API) healthCheck(ctx context.Context) error {
	vals := a.getCtxValues(ctx)
	log := a.log.With("id", vals.ID)

	log.Info("health called")
	// TODO: connection to database, ...
	return nil
}

func (a *API) healthHandler(w http.ResponseWriter, r *http.Request) {
	err := a.healthCheck(r.Context())
	if err != nil {
		http.Error(w, "bad health", http.StatusInternalServerError)
		return
	}

	fmt.Fprintln(w, "OK")
}

func (a *API) getCtxValues(ctx context.Context) *ctxValues {
	vals, ok := ctx.Value(ctxKey).(*ctxValues)
	if !ok || vals == nil {
		vals = &ctxValues{
			ID: "<unknown>",
			// Log: a.log,
		}
	}

	return vals
}

type ctxValues struct {
	ID    string
	Login string
	// TODO: Authenticated user ...
	// Logger *slog.Logger // debate ☺
}

var ctxKey contextKeyType = 7

type contextKeyType int

/* External routers
- gorilla/mux
- chi
- ...
*/

func ctxKeyTypeExample() {
	ctx := make(map[any]any)
	type ctxKey1 int
	key1 := ctxKey1(1)

	ctx[key1] = "A"

	// another package
	type ctxKey2 int
	key2 := ctxKey2(1)
	ctx[key2] = "B"

	fmt.Println(ctx)
}
