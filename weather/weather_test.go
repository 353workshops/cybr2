package main

import (
	"fmt"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_healthHandler(t *testing.T) {
	a := API{log: slog.Default()}

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost, "http://localhost:8080/health", nil)

	// Problem: Bypass middleware
	a.healthHandler(w, r)

	resp := w.Result()
	require.Equal(t, http.StatusOK, resp.StatusCode)
}

func alias() {
	// type Int int // new type
	type Int = int // type alias

	// type alias is for refactoring
	// type User = users.User

	v1 := Int(3)
	v2 := 2

	fmt.Println(v1 + v2)
}
