package main

import (
	"fmt"
	"time"
)

type Record struct {
	StationID string `json:"station_id"`
	Time      time.Time
	Wind      float64 // MPH
	Temp      float64 // Fahrenheit
}

func (r Record) Validate() error {
	if r.StationID == "" {
		return fmt.Errorf("missing station ID")
	}

	if r.Time.IsZero() {
		return fmt.Errorf("missing time")
	}

	// record is 253mph
	if r.Wind < 0 || r.Wind > 300 {
		return fmt.Errorf("bad wind")
	}

	// lowest record: -128.6 highest record: 134f
	if r.Temp < -150 || r.Temp > 150 {
		return fmt.Errorf("bad temp")
	}

	return nil
}
