package main

import (
	"fmt"
	"log"
	"net/http"
	"sync"
)

func main() {

	urls := []string{
		"https://go.dev",
		"https://cyberark.com",
		"https://example.com/no/such/page",
	}
	var wg sync.WaitGroup

	wg.Add(len(urls))
	for _, url := range urls {
		// wg.Add(1)
		url := url
		go func() {
			defer wg.Done()
			urlStatus(url)
		}()
	}

	wg.Wait() // blocked until counter reaches 0

	p := Payment{
		From:   "Wile. E. Coyote",
		To:     "ACME",
		Amount: 12557,
	}
	p.Pay()
	p.Pay()
}

func (p *Payment) Pay() {
	p.once.Do(p.pay)
}

func (p *Payment) pay() {
	d := p.Amount / 100
	c := p.Amount % 100
	fmt.Printf("%s -> $%d.%d -> %s\n", p.From, d, c, p.To)
}

type Payment struct {
	once sync.Once
	/*
		mu sync.Mutex
		done bool
	*/

	From   string
	To     string
	Amount int // cents
}

func urlStatus(url string) {
	resp, err := http.Get(url)
	if err != nil || resp.StatusCode != http.StatusOK {
		log.Printf("ERROR: %q", url)
		return
	}

	log.Printf("INFO: %q OK", url)

}
