package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	// Handling different types
	var evt Event
	if err := json.Unmarshal(loginEvt, &evt); err != nil {
		fmt.Println("ERROR:", err)
		return
	}
	fmt.Printf("evt: %#v\n", evt)

	if evt.Type == "login" {
		var le LoginEvent
		if err := json.Unmarshal(evt.Payload, &le); err != nil {
			fmt.Println("ERROR:", err)
			return
		}
		fmt.Printf("login event: %#v\n", le)
	}

	// Streaming
	var many = `
	{
		"login": "elliot",
		"server": "srv1"
	}
	
	{
		"login": "elliot",
		"server": "srv2"
	}
	`

	dec := json.NewDecoder(strings.NewReader(many))
loop:
	for {
		var evt LoginEvent
		err := dec.Decode(&evt)

		switch {
		case errors.Is(err, io.EOF):
			break loop
			// fallthrough
		case err != nil:
			fmt.Println("ERROR:", err)
			return
		default:
			fmt.Println(evt)
		}
	}

	enc := json.NewEncoder(os.Stdout)
	le := LoginEvent{"elliot", "srv3"}
	for i := 0; i < 3; i++ {
		if err := enc.Encode(le); err != nil {
			fmt.Println("ERROR:", err)
			return
		}
	}

	// HTTP chunked encoding
}

type LoginEvent struct {
	Login  string
	Server string
}

var loginEvt = []byte(`
{
	"type": "login",
	"payload": {
		"login": "elliot",
		"server": "srv1"
	}
}`)

/*
# Handling different messages
- json.RawMessage: Good if you have nested types and type indicator
- mapstructure: for everything else

# Streaming
*/

type Event struct {
	Type    string
	Payload json.RawMessage
}

/*
{
	"type": "login",
	"payload": {
		"login": "elliot",
		"server": "srv1"
	}
}

{
	"type": "access",
	"payload": {
		"login": "elliot",
		"uri": "/etc/passwd"
	}
}
*/
