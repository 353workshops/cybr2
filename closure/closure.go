package main

import "fmt"

func main() {
	inc2 := makeInc(2)    // inc2 "remembers" that n = 2
	fmt.Println(inc2(10)) // 12

	acct := makeAccount()
	fmt.Println(acct(100)) // 100
	fmt.Println(acct(-30)) // 70
}

func makeInc(n int) func(int) int {

	fn := func(v int) int {
		return v + n // Where does "n" come from? (closure)
	}

	return fn
}

func makeAccount() func(int) int {
	balance := 0

	return func(n int) int {
		balance += n
		return balance
	}
}
