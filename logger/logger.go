package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	l := New(os.Stdout) // has Sync method
	l.Info("Go is awesome!")

	var buf bytes.Buffer // does not have Sync method
	lm := New(&buf)
	lm.Info("Even more than I thought")
	fmt.Println(buf.String())
}

/* Break API, restrict types
type WriteSyncer interface {
	io.Writer
	Sync() error
}
*/

type syncer interface {
	Sync() error
}

func (l Logger) Info(message string) {
	fmt.Fprintf(l.w, "INFO: %s\n", message)
	//if s, ok := l.w.(syncer); ok {
	// if l.s != nil
	l.s.Sync()
}

type nopSyncer struct{}

func (s nopSyncer) Sync() error { return nil }

// func New(w WriteSyncer) Logger {
func New(w io.Writer) Logger {
	l := Logger{w: w, s: nopSyncer{}}
	if s, ok := w.(syncer); ok {
		l.s = s
	}

	return l
}

type Logger struct {
	w io.Writer
	//w WriteSyncer
	s syncer
}
