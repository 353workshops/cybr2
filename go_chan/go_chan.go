package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	go fmt.Println("goroutine")
	fmt.Println("main")

	for i := 0; i < 5; i++ {
		// Solution 3: Use go 1.22+
		// Solution 2: Use loop local variable
		i := i // i shadows "i" from line 12
		go func() {
			fmt.Println("g:", i)
		}()
		/* Solution 1: Pass as parameter
		go func(n int) {
			fmt.Println("g:", n)
		}(i)
		*/
		/*
			go func() {
				fmt.Println("g:", i) // BUG: All goroutines use the same i from line 12
			}()
		*/
	}

	time.Sleep(time.Millisecond)

	ch := make(chan string)
	/*
		fn := func() { ch <- "hi"}
		go fn()
	*/
	go func() {
		ch <- "hi" // send
	}()
	val := <-ch // receive
	fmt.Println(val)

	fmt.Println(sleepSort([]int{30, 10, 20})) // [10 20 30]

	go func() {
		defer close(ch)

		for i := 0; i < 5; i++ {
			ch <- fmt.Sprintf("msg #%d", i)
		}
	}()

	/*
		for {
			msg, ok := <-ch
			if !ok {
				break
			}
			fmt.Println(msg)
		}
	*/
	for msg := range ch {

		fmt.Println(msg)
	}

	m := <-ch // ch is closed
	fmt.Printf("m: %q\n", m)

	m, ok := <-ch // ch is closed
	fmt.Printf("m: %q (ok=%v)\n", m, ok)
	// ch <- "sup?"

	fmt.Println("starting future")
	fut := Future(func() int { return worker(3) })
	fmt.Println("doing other stuff")
	out := <-fut
	fmt.Println("got:", out)

	fut2 := Future2(worker2)
	fmt.Println(<-fut2)

	ch1, ch2 := make(chan int, 1), make(chan int, 1)
	go func() {
		time.Sleep(10 * time.Millisecond)
		ch1 <- 1
	}()
	go func() {
		time.Sleep(20 * time.Millisecond)
		ch2 <- 2
	}()

	/*
		done := make(chan bool)
		go func() {
			time.Sleep(time.Millisecond)
			close(done)
		}()
	*/
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Millisecond)
	defer cancel()

	select {
	case v := <-ch1:
		fmt.Println("ch1:", v)
	case v := <-ch2:
		fmt.Println("ch2:", v)
	case <-ctx.Done():
		fmt.Println("context expired")
		// ch := time.After(time.Millisecond)
		// case <-ch:
		/*
			case <-time.After(5 * time.Millisecond):
				fmt.Println("timeout")
			case <-done:
				fmt.Println("cancel")
		*/
	}

	pool := make(chan bool, 3)
	for i := 0; i < 10; i++ {
		go func() {
			time.Sleep(time.Microsecond)
			pool <- true // enter semaphore
			fmt.Println("in")

			defer func() {
				<-pool
				fmt.Println("out")
			}()
		}()
	}

	time.Sleep(time.Millisecond)
}

func worker2() (int, error) {
	time.Sleep(10 * time.Millisecond)
	return 7, nil
}

func worker(n int) int {
	time.Sleep(10 * time.Millisecond)
	return n * 7
}

type Result struct {
	Value int
	Err   error
}

func Future2(fn func() (int, error)) chan Result {
	ch := make(chan Result, 1) // buffered channel to avoid goroutine leak

	go func() {
		var r Result
		r.Value, r.Err = fn()
		ch <- r
	}()

	return ch
}

func Future(fn func() int) chan int {
	ch := make(chan int, 1) // buffered channel to avoid goroutine leak

	go func() {
		ch <- fn()
	}()

	return ch
}

/* Channel semantics
- Send/receive to/from a channel will block until opposite operation (*)
	- Channel with buffer "n" has "n" non-blocking sends
- Receive from a closed channel will return zero value without blocking
- Send/close to a closed channel will panic
- Send/receive to/from a nil channel will block forever
*/

/*
	Exercise: sleepSort

for every "n" in values, start a goroutine that will
  - sleep n milliseconds
  - send n over a channel

collect values from channel into a slice and return it

time.Sleep(time.Duration(n) * time.Millisecond)
*/
func sleepSort(values []int) []int {
	ch := make(chan int)
	for _, n := range values {
		n := n
		go func() {
			time.Sleep(time.Duration(n) * time.Millisecond)
			ch <- n
		}()

	}

	var sorted []int
	for range values {
		n := <-ch
		sorted = append(sorted, n)
	}
	return sorted
}
