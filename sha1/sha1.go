package main

import (
	"compress/gzip"
	"crypto/sha1"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	// GoLand sha1/http.log.gz
	fmt.Println(fileSHA1("http.log.gz"))
	fmt.Println(fileSHA1("sha1.go"))

	// aside: Scope
	a := 1
	{
		// a := 2 // new variable, shadows a from line 18
		a = 2 // assing to a from line 17
		fmt.Println("inner:", a)
	}
	fmt.Println("outer:", a)
}

func fileSHA1(fileName string) (string, error) {
	// cat http.log.gz
	file, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	defer file.Close()
	// Idiom: acquire resource, check for error, defer release

	var r io.Reader = file

	if strings.HasSuffix(fileName, ".gz") {
		// | gunzip
		// r, err := gzip.NewReader(file) // BUG: r shadows "r" from line 27
		r, err = gzip.NewReader(file)
		if err != nil {
			return "", err
		}
		// fmt.Println("gzip:", r)
	}

	w := sha1.New()
	if _, err := io.Copy(w, r); err != nil {
		return "", err
	}

	sig := w.Sum(nil)
	return fmt.Sprintf("%x", sig), nil
}
