package unter

import (
	"fmt"

	"github.com/cyberark/unter/backoffice"
)

// Ride is a ride summary.
type Ride struct {
	ID       string
	Distance float64 // km
	Shared   bool
}

// Validate validates that a ride is valid.
func (r Ride) Validate() error {
	if r.ID == "" {
		return fmt.Errorf("empty ride ID")
	}

	if r.Distance <= 0 || r.Distance > 3_000 {
		return fmt.Errorf("%#v - bad distance", r)
	}

	return nil
}

// Fare returns the ride fare.
func (r Ride) Fare() float64 {
	return backoffice.RideFare(r.Distance, r.Shared)
}
