package unter

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"
)

func TestRide_Fare(t *testing.T) {
	r := Ride{Distance: 3, Shared: false}
	f := r.Fare()
	require.Equal(t, 4.5, f)
}

func FuzzRide_Validate(f *testing.F) {
	f.Add("0", 36564.0)

	fn := func(t *testing.T, id string, distance float64) {
		r := Ride{
			ID:       id,
			Distance: distance,
		}

		err := r.Validate()
		if id == "" || distance <= 0 || distance > 3_000 {
			if err == nil {
				t.Fatalf("no error for %q %f", id, distance)
			}
			return
		}

		if err != nil {
			t.Fatalf("unexpected error: %q %f - %s", id, distance, err)

		}
	}

	f.Fuzz(fn) // will run fn with random values for id & distance
}

/*
var validateCases = []struct { //anonymous struct
	name     string
	id       string
	distance float64
	valid    bool
}{
	{"valid ride", "one", 3.4, true},
	{"bad distance", "one", -3.4, false},
	{"empty ID", "", 3.4, false},
}
*/

// On Jenkins use BUILD_NUMBER
var isCI = os.Getenv("CI") != ""

func assertCI(t *testing.T) {
	t.Helper()

	if !isCI {
		t.Skip("not in CI")
	}
}

func TestInCI(t *testing.T) {
	assertCI(t)

	t.Log("CI test")
}

type testCase struct {
	Name     string
	ID       string
	Distance float64
	Valid    bool
}

func loadValidateCases(t *testing.T) []testCase {
	t.Helper()

	file, err := os.Open("testdata/validate_cases.yml")
	require.NoError(t, err)
	defer file.Close()

	dec := yaml.NewDecoder(file)
	var cases []testCase
	err = dec.Decode(&cases)
	require.NoError(t, err)

	return cases
}

// Exercise: Load test cases from testdata/validate_cases.yml
// instead from in-memory validateCases
// gopkg.in/yaml.v3
func TestRide_ValidateTable(t *testing.T) {
	for _, tc := range loadValidateCases(t) {
		t.Run(tc.Name, func(t *testing.T) {
			r := Ride{tc.ID, tc.Distance, true}
			err := r.Validate()
			if !tc.Valid && err == nil {
				t.Fatalf("%#v should be invalid", r)
			}
			if tc.Valid && err != nil {
				t.Fatalf("#%v should be valid", r)
			}
		})
	}
}

func TestRide_Validate(t *testing.T) {
	r := Ride{
		ID:       "two",
		Distance: 12.3,
		Shared:   true,
	}
	t.Log("checking valid ride")
	err := r.Validate()
	require.NoErrorf(t, err, "%#v is invalid (should be valid)", r)

}

// go help testflag
// go test -v

// A -> C v2.1
// B -> C 2.2

// semantic versioning: 1.2.3
// 1 major: breaking changes
// 2 minor: add new features (backward compatible)
// 3 patch: bug fixes
// In Go major version is a new import path
// Go users git tags/branches for versions (v2.3.4)

/* Skipping/grouping tests
- Environment variables
- -short flag to "go test", check with testing.Short()
- Build tags
	Add at top of file: //go:build ui_test
	Run with "go test -tags ui_test"
*/
