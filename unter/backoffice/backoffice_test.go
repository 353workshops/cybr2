package backoffice

import (
	"testing"

	"github.com/stretchr/testify/require"
)

var feeCases = []struct {
	name     string
	distance float64
	shared   bool
	fare     float64
}{
	{"regular", 3, false, 4.5},
	{"shared", 3, true, 4.05},
	{"round up", 2.1, false, 4.5},
}

func TestRideFare(t *testing.T) {
	for _, tc := range feeCases {
		t.Run(tc.name, func(t *testing.T) {
			fare := RideFare(tc.distance, tc.shared)
			require.Equal(t, tc.fare, fare)
		})
	}

}
