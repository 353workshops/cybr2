package backoffice

import "math"

const initialFare = 1.5

// RideFare calculate the ride fee
// Fee start with $1.5 and adds a $1 per mile.
// If the ride is shared there's a 10% discount.
func RideFare(distance float64, shared bool) float64 {
	fare := initialFare
	fare += math.Ceil(distance)
	if shared {
		fare *= 0.9
	}

	return fare
}
