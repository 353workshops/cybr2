package main

import (
	"flag"
	"fmt"
	"log/slog"
	"net/http"
	"os"
	"path"

	"expvar"
)

var (
	mVersion = expvar.NewString("version")
	mCalls   = expvar.NewInt("health.calls")
)

func healthHandler(w http.ResponseWriter, r *http.Request) {
	mCalls.Add(1)
	// TODO
	fmt.Fprintln(w, "OK")
}

var (
	opts struct {
		ShowVersion bool
	}
	Version = "<dev>"
)

// Configuration: defaults < config file < environment < command line
// defaults: in code
// config file: YAML/TOML, viper, ardanlabs/conf
// environment: os.environ, cobra
// command line: flag, cli, cobra, ardanlabs/conf

// IMPORTANT: Validate configuration when starting
// playground/validator, cue, custom logic

// Logging: slog (Go >= 1.22), log, zap, logrus ...
// Metrics: expvar, prometheus, opentelemetry (otel), ...

func main() {
	flag.BoolVar(&opts.ShowVersion, "version", false, "show version & exit")
	flag.Parse()

	if opts.ShowVersion {
		fmt.Printf("%s version %s\n", path.Base(os.Args[0]), Version)
		os.Exit(0)
	}

	mVersion.Set(Version)

	http.HandleFunc("/health", healthHandler)

	addr := os.Getenv("ADDR")
	if addr == "" {
		addr = ":8080"
	}

	// log.Printf("INFO: server starting on %s", addr)
	slog.Info("server starting", "address", addr)
	if err := http.ListenAndServe(addr, nil); err != nil {
		//log.Printf("ERROR: can't run - %s", err)
		slog.Error("can't run", "error", err)
		os.Exit(1)
	}
}
