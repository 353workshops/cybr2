package unter_test

import (
	"fmt"

	"github.com/cyberark/unter"
)

func ExampleRide_Validate() {
	ride := unter.Ride{
		ID:       "one",
		Distance: 3.4,
		Shared:   false,
	}
	fmt.Println(ride.Validate())

	ride.ID = ""
	fmt.Println(ride.Validate())

	// Output:
	// <nil>
	// empty ride ID
}
