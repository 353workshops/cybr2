package main

import "fmt"

func main() {
	var a any // go < 1.18 interface{}

	a = 1
	fmt.Println(a)
	// fmt.Println(a + 1)

	a = "Hi"
	fmt.Println(a)

	s := a.(string) // type assertion
	fmt.Println("s: " + s)

	// i := a.(int)// will panic
	//a = 7
	i, ok := a.(int) // comma ok
	if ok {
		fmt.Println("i:", i)
	} else {
		fmt.Printf("ERROR: not an int - %T\n", a)
	}

	// fmt.Println(string(73)) // I
}

// Thought experiment: sorting

type Sortable interface {
	Less(i, j int) bool
	Len() int
	Swap(i, j int)
}

func Sort(s Sortable) {
	//...
}
